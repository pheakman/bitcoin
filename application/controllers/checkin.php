<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->library( 'session');
        $this->load->database();
        $this->load->model('Checkin_model');
        $this->load->model('Message_model');
    }


    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } else {
            $id = $this->session->userdata('user_id');
            $this->db->from('users');
            $this->db->where('id', $id );
            $user = $this->db->get()->result()[0];
            $this->data['data'] = array(
                'username'      => $user->username,
                'email'         => $user->email,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'company'       => $user->company,
                'phone'         => $user->phone,
                'sex'           => $user->sex,
                'age'           => $user->age,
                'homeno'        => $user->homeno,
                'street'        => $user->street,
                'phum'          => $user->phum,
                'sangkat'       => $user->sankat,
                'khan'          => $user->khan,
                'city'          => $user->city,
                'img_url'       => $user->img_url,
                'verify'        => $user->verify
            );
            $this->data["mgs"] =$this->session->userdata('mgs');
            $this->data['title'] = "Check-In";
            $this->data['check_mgs'] = $this->session->flashdata('check_mgs');
            $this->data['msg_count'] = count($this->Message_model->getList($id));
            $this->data['username'] = $this->session->userdata('identity');
            $this->data['email'] = $this->session->userdata('email');
            $this->load->view('checkin/check-in', $this->data);
        }
    }

    public function insert()
    {
        $id = $this->session->userdata('user_id');
        $img_url = "";

        $data = array(
            'user_id' => $id,
            'amount'      => $this->input->post('amount'),
            'date' => date("Y-m-d H:i:s")

        );
        if($_FILES['invoice']['name']) {
            $data['invoice_url'] = $this->upload();
        }
        // check to see if we are updating the user
        if($this->Checkin_model->insert($data))
        {
            $this->session->set_flashdata('check_mgs', 'The Invoice sumit suucessfull!');
            redirect('checkin');
        } else {
            redirect('checkin');
        }
    }

    function upload(){
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $timestamp = time() - date('Z');
        $config['file_name'] =  $timestamp.'-'.$_FILES['invoice']['name'];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('invoice'))
        {
            $error = array('error' => $this->upload->display_errors());
            return "";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            return $data['upload_data']['file_name'];
        }
    }

}
