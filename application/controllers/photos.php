<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->library( 'session');
        $this->load->database();
        $this->load->model('ion_auth_model');
        $this->load->model('Checkin_model');
        $this->load->model('Message_model');
    }


    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } else {
            $id = $this->session->userdata('user_id');
            $this->db->from('users');
            $this->db->where('id', $id );
            $user = $this->db->get()->result()[0];
            $this->data['data'] = array(
                'img_url'       => $user->img_url,
                'verify'        => $user->verify
            );

            // Get Check In photos
            $photo = $this->Checkin_model->getList($id);
            $this->data['check_in'] = $photo;
            $this->data['title'] = "My Photos";
            $this->data['msg_count'] = count($this->Message_model->getList($id));
            $this->data['username'] = $this->session->userdata('identity');
            $this->data['email'] = $this->session->userdata('email');
            $this->load->view('photos/myphotos', $this->data);
        }
    }
}
