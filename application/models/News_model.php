<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{
    /**
     * Holds an array of tables used
     *
     * @var array
     **/

    public $tables = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('date');
        $this->load->library('session');
    }

    function getList($id){
        $this->db->from('5om6oba3b_posts');
        $this->db->where('post_type', 'post' );
        $this->db->where('post_status', 'publish' );
        $news = $this->db->get()->result_array();

        $id = $this->session->userdata('user_id');
        $this->db->from('read_news');
        $this->db->where('user_id', $id );
        $read = $this->db->get()->result_array();

        for($i=0; $i<count($news); $i++){
            $news[$i]["read"] = false;
            foreach ($read as $r) {
                if($news[$i]["ID"]==$r["news_id"]){
                    $news[$i]["read"] = true;
                }
            }
        }

        return $news;
    }

    function readMessage($item){
        return $this->db->insert('read_news', $item);
    }
}