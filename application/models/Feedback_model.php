<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends CI_Model
{
    /**
     * Holds an array of tables used
     *
     * @var array
     **/

    public $tables = array();

    public $amount;
    public $invoice_url;


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->config('ion_auth', TRUE);
        $this->load->helper('cookie');
        $this->load->helper('date');

        // initialize db tables data
        $this->tables = $this->config->item('tables', 'feedback');


    }

    function insert($item)
    {
//        $this->db->insert('feedback', $item);
        return $this->db->insert('feedback', $item);
    }
    
    function getList($userid){
        $this->db->from('feedback');
        $this->db->where('user_id', $userid );
        $query = $this->db->get();
        return $query->result_array();
    }
}