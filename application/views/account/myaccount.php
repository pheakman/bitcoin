<?php
define("ROOT_PATH", dirname(__DIR__));
include_once ROOT_PATH . "/template/header.php";
include_once ROOT_PATH . "/template/menu.php";
?>

<h1>My Account</h1>
<p class="success"><?php echo $account_mgs; ?></p>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget widget-tabs widget-tabs-gray widget-tabs-double-2 border-bottom-none">
        <div class="widget-body">
                <div class="tab-content">
                    <div class="tab-pane active widget-body-regular padding-none" id="overview">
                        <div class="row-fluid row-merge">
                            <div class="span12 containerBg innerTB">
                                <div class="innerLR">
                                    <div class="row-fluid innerB">
                                        <div class="span12">
                                             <?php echo form_open_multipart('account/update');?>
                                            <!-- About -->
                                            <div class="widget widget-heading-simple widget-body-gray margin-none">
                                                <div class="widget-head"><h4 class="heading glyphicons user"><i></i><?php echo $username; ?></h4></div>
                                                <div class="widget-body">
                                                    <div class="row-fluid innerB">
                                                        <div class="span2">
<!--                                                            <img src="http://dummyimage.com/200x200/232323/ffffff&amp;text=photo" alt="Avatar">-->
<!--                                                            <input type='file'  onchange="readURL(this);" />-->
                                                            <div class="col-md-12 fileinput fileinput-new blog-center" data-provides="fileinput">
		                                                        <span class="">
		                                                            <img style="width:100%;margin-bottom:10px;" id="preview" alt="User Pic" src="<?php echo ($data['img_url'])?base_url().'assets/uploads/'.$data['img_url']:'http://aminoapps.com/static/img/user-icon-placeholder.png';?>" class="">
		                                                        </span>
		                                                        <div class="fileupload fileupload-new margin-none" style="text-align: center;" data-provides="fileupload">
																  	<span class="btn btn-default btn-file">
																  		<span class="fileupload-new">Select file</span>
																  		<span class="fileupload-exists">Change</span>
																  		<input class="margin-none" type="file" name="image" onchange="previewImage(this)" />
																  	<span class="fileupload-preview"></span>
																  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
																</div>
		                                                        <span class="fileinput-filename"></span>
	                                                        </div>
                                                        </div>
                                                        <div class="span10">
                                                            <!-- Column -->
                                                            <div class="span6">
                                                                <div class="innerR">
                                                                    <label class="strong">Username</label>
                                                                    <?php echo form_error('username', '<div class="error">', '</div>'); ?>
                                                                    <input type="text" name="username" class="input-block-level" placeholder="Your Username" value="<?php echo $data['username'];?>" required/>
                                                                    <label class="strong">Email</label>
                                                                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                                                    <input type="text" name="email" class="input-block-level" placeholder="Your Email Address" value="<?php echo $data['email'];?>" required/>
                                                                    <label class="strong">Phone</label>
                                                                    <input type="text" name="phone" class="input-block-level" placeholder="Your Phone Number" value="<?php echo $data['phone'];?>" required/>
                                                                    <label class="strong">Sex</label>
                                                                    <input type="text" name="sex" class="input-block-level" placeholder="Sex" value="<?php echo $data['sex'];?>" required/>
                                                                    <label class="strong">Age</label>
                                                                    <input type="text" name="age" class="input-block-level" placeholder="Age" value="<?php echo $data['age'];?>" required/>
                                                                    <label class="strong">Sponser</label>
                                                                    <input type="text" name="sponser" class="input-block-level" placeholder="Sponser" value="<?php echo $data['sponser'];?>" required/>
                                                                    <button type="submit" class="btn btn-icon-stacked  btn-success glyphicons user_add btn-user-save"><i></i>Save</button>
                                                                </div>
                                                            </div>
                                                            <!-- // Column END -->

                                                            <!-- Column -->
                                                            <div class="span6">
                                                                <div class="innerL">
                                                                    <label class="strong">Home No</label>
                                                                    <input type="text" name="homeno" class="input-block-level" placeholder="Your Home Number" value="<?php echo $data['homeno'];?>" required/>
                                                                    <label class="strong">Street</label>
                                                                    <input type="text" name="street" class="input-block-level" placeholder="Your Street Number" value="<?php echo $data['street'];?>" required/>
                                                                    <label class="strong">Phum</label>
                                                                    <input type="text" name="phum" class="input-block-level" placeholder="Your Phum" value="<?php echo $data['phum'];?>" required/>
                                                                    <label class="strong">Sankat</label>
                                                                    <input type="text" name="sankat" class="input-block-level" placeholder="Your Sankat" value="<?php echo $data['sankat'];?>" required/>
                                                                    <label class="strong">Khan</label>
                                                                    <input type="text" name="khan" class="input-block-level" placeholder="Your Khan" value="<?php echo $data['khan'];?>" required/>
                                                                    <label class="strong">City</label>
                                                                    <input type="text" name="city" class="input-block-level" placeholder="Your City" value="<?php echo $data['city'];?>" required/>
                                                                    <label class="strong">County</label>
                                                                    <input type="text" name="country" class="input-block-level" placeholder="Your Country" value="<?php echo $data['country'];?>" required/>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // About END -->
                                        </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    <script type="text/javascript">
        function previewImage(input) {
            var preview = document.getElementById('preview');
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    preview.setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <!-- // Widget END -->
</div>
</div>
<!-- // Content END -->
<?php
include_once ROOT_PATH . "/template/menu-footer.php";
include_once ROOT_PATH . "/template/footer.php";
?>
