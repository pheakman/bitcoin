<?php
define("ROOT_PATH", dirname(__DIR__));
include_once ROOT_PATH . "/template/header.php";
include_once ROOT_PATH . "/template/menu.php";
?>

    <h1>Check-In</h1>
    <p class="success"><?php echo $check_mgs; ?></p>
    <div class="innerLR">
    <!-- Widget -->
    <div class="widget widget-tabs widget-tabs-gray widget-tabs-double-2 border-bottom-none">
        <div class="widget-body">
            <div class="tab-content">
                <div class="tab-pane active widget-body-regular padding-none" id="overview">
                    <div class="row-fluid row-merge">
                        <div class="span12 containerBg innerTB">
                            <div class="innerLR">
                                <div class="row-fluid innerB">
                                    <div class="span12">
                                        <?php echo form_open_multipart('checkin/insert');?>
                                          <!-- About -->
                                          <div class="widget widget-heading-simple widget-body-gray margin-none">
                                              <div class="widget-body">
                                                  <div class="row-fluid innerB">
                                                      <div class="span10">
                                                          <!-- Column -->
                                                          <div class="span6">
                                                              <div class="innerR">
                                                                  <label class="strong">Amount</label>
                                                                  <?php echo form_error('username', '<div class="error">', '</div>'); ?>
                                                                  <div class="input-prepend input-append">
                                                                          <span class="add-on">$</span>
                                                                      <input class="span12" name="amount" id="appendedPrependedInput" type="text" placeholder="100" required/>
                                                                          <span class="add-on">.00</span>
                                                                  </div>
                                                                  <label class="strong">Upload Invoice</label>
                                                                  <div class="col-md-12 fileinput fileinput-new blog-center" data-provides="fileinput">
                                                                      <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                                                          <img id="preview" src="" alt="Invoice" style="clear:both;margin-bottom: 10px;width: 100%;">
                                                                          <span class="btn btn-default btn-file">
                                                                              <span class="fileupload-new">Select file</span>
                                                                              <span class="fileupload-exists">Change</span>
                                                                              <input class="margin-none" type="file" name="invoice" onchange="previewImage(this)" />
                                                                          <span class="fileupload-preview"></span>
                                                                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                                                                      </div>
                                                                      <span class="fileinput-filename"></span>
                                                                  </div>
                                                              </div>
                                                              <button style="clear:both;margin-top: 10px;" type="submit" class="btn btn-success btn-submit"><i class="icon-check"></i> Submit</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function previewImage(input) {
            var preview = document.getElementById('preview');
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    preview.setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    </div>
    <!-- // Content END -->
<?php
include_once ROOT_PATH . "/template/menu-footer.php";
include_once ROOT_PATH . "/template/footer.php";
?>
<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/22/16
 * Time: 12:54 AM
 */
