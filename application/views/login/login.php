
<!-- Wrapper -->
<div id="login">

    <div class="container">

        <div class="wrapper">

            <h1 class="glyphicons lock">SOMBATH TEAM <i></i></h1>

            <!-- Box -->
            <div class="widget widget-heading-simple widget-body-gray">
                <p class="success"><?php echo $success_msg;?> </p>
                <div class="widget-body">
                    <!-- Form -->
                    <form method="post" action="#">
                        <label>Username</label>
                        <?php echo form_error('identity'); ?>
                        <input name="identity" type="text" class="input-block-level" placeholder="Your Username"
                               required/>
                        <label>Password <a class="password" href="forgot_password">forgot it?</a></label>
                        <?php echo form_error('password'); ?>
                        <input name="password" type="password" class="input-block-level margin-none"
                               placeholder="Your Password" required/>
                        <div class="separator bottom"></div>
                        <div class="row-fluid">
                            <div class="span8">
                                <div class="uniformjs"><label class="checkbox"><input type="checkbox" name="remember" value="remember-me">Remember me</label></div>
                            </div>
                            <div class="span4 center">
                                <button class="btn btn-block btn-inverse" type="submit">Sign in</button>
                            </div>
                        </div>
                    </form>
                    <!-- // Form END -->

                </div>
            </div>
            <!-- // Box END -->

            <div class="innerT center">
                <a href="<?php echo base_url('register'); ?>" class="btn btn-icon-stacked btn-block btn-success glyphicons user_add"><i></i><span>Don't have an account?</span><span class="strong">Sign up</span></a>
            </div>

        </div>

    </div>

</div>
