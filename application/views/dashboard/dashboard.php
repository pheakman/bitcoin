<?php
define("ROOT_PATH", dirname(__DIR__));
include_once ROOT_PATH . "/template/menu.php";
?>
            <h1>Dashboard</h1>
            <!-- Stats Widgets -->
            <div class="row-fluid row-merge border-top" style="border-bottom: 1px solid #efefef">
                <div class="span3">
                    <div class="innerAll padding-bottom-none-phone">
                        <a href="" class="widget-stats widget-stats-default widget-stats-4">
                            <span class="txt">Ratings</span>
                            <span class="count">4.3</span>
                            <span class="glyphicons cup"><i></i></span>
                            <div class="clearfix"></div>
                            <i class="icon-play-circle"></i>
                        </a>
                    </div>
                </div>
                <div class="span3">
                    <div class="innerAll padding-bottom-none-phone">
                        <a href="" class="widget-stats widget-stats-primary widget-stats-4">
                            <span class="txt">Progress</span>
                            <span class="count">58%</span>
                            <span class="glyphicons refresh"><i></i></span>
                            <div class="clearfix"></div>
                            <i class="icon-play-circle"></i>
                        </a>
                    </div>
                </div>
                <div class="span3">
                    <div class="innerAll padding-bottom-none-phone">
                        <a href="" class="widget-stats widget-stats-gray widget-stats-4">
                            <span class="txt">Signups</span>
                            <span class="count">3<span>Today</span></span>
                            <span class="glyphicons user"><i></i></span>
                            <div class="clearfix"></div>
                            <i class="icon-play-circle"></i>
                        </a>
                    </div>
                </div>
                <div class="span3">
                    <div class="innerAll padding-bottom-none-phone <?php echo ($count_booking<=7)?'alert_book':'';?>">
                        <a href="" class="widget-stats widget-stats-2">
                            <span class="count"><?php echo $count_booking; ?></span>
                            <span class="txt">Bookings</span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- // Stats Widgets END -->

            <div class="heading-buttons">
                <h2 class="heading pull-left"><i class="icon-bar-chart icon-fixed-width text-primary"></i> Partner With
                    Us</h2>
                <div class="buttons">
                    <a href="#" class="btn btn-primary"><?php echo date('d M Y'); ?></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="innerLR innerT">

                <!-- Website Traffic Chart -->
                <div class="widget widget-body-gray">
                    <div class="widget-body">
                        <div id="chart_simple" style="height: 253px;"></div>
                    </div>
                </div>
                <!-- // Website Traffic Chart END -->

            </div>
            <p class="separator text-center"><i class="icon-ellipsis-horizontal icon-3x"></i></p>
            <div class="innerLR innerB">

                <div class="row-fluid">
                    <div class="span9 tablet-column-reset">

                        <!-- Widget -->
                        <h2 class="margin-none separator bottom"><i class="icon-time text-primary icon-fixed-width"></i>
                            Recent News</h2>


                        <div class="separator bottom"></div>
                        <!-- // Stats Widgets END -->
                        <div class="widget widget-heading-simple widget-body-simple">
                            <div class="widget-body">
                                <div
                                    class="widget widget-tabs widget-tabs-icons-only-2 widget-activity margin-none widget-tabs-gray">
                                    <div class="widget-body">
                                        <div class="tab-content">
                                            <!-- Filter Messages Tab -->
                                            <div class="tab-pane active" id="filterMessagesTab">
                                                <ul class="list">

                                                    <?php
                                                    $i = 1;
                                                    foreach($news as $row) { ?>
                                                        <!-- Activity item -->
                                                        <a href="#myModal<?php echo $i; ?>" data-toggle="modal" data-id="<?php echo $row["ID"];?>" id="news" class="<?php if($row["read"]==1) echo "message-read";?>">
                                                        <li class="double">
                                                                <span class="glyphicons activity-icon icon-bullhorn"><i></i></span>
                                                                <span class="ellipsis">
                                                                    <?php echo $row["post_title"]; ?>
                                                                    <span class="meta">on <?php
                                                                        $date = new DateTime($row["post_date"]);
                                                                        echo date_format($date, 'M d, Y H:i a');; ?></span>
                                                                </span>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        </a>
                                                        <!-- // Activity item END -->
                                                        <div class="modal hide fade" id="myModal<?php echo $i; ?>">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h3><?php echo $row["post_title"]; ?></h3>
                                                          </div>
                                                          <div class="modal-body">
                                                            <p><?php echo nl2br(trim($row["post_content"])); ?></p>
                                                          </div>
                                                        </div>
                                                    <?php $i++; } ?>
                                                    <!-- Activity item -->
                                                </ul>
                                                
                                            </div>
                                            <!-- // Filter Messages Tab END -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p class="separator text-center"><i class="icon-ellipsis-horizontal icon-3x"></i></p>
                        <!-- Chat -->
                        <h2 class="margin-none separator bottom"><i
                                class="icon-comments text-primary icon-fixed-width"></i> Send us your message</h2>
                        <div class="widget widget-heading-simple widget-body-white widget-chat">
                            <div class="chat-controls" style="border-top: 1px solid #efefef;">
                                <div class="innerLR">
                                    <?php echo form_open('dashboard/feedback', array('id' => 'feedback')); ?>
                                        <div class="row-fluid">
                                            <div class="span10">
                                                <input type="text" name="message" class="input-block-level margin-none"
                                                       placeholder="Type your message .."/>
                                            </div>
                                            <div class="span2">
                                                <button type="submit" class="btn btn-block btn-primary">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <p class="success"><?php echo $feedback_mgs; ?></p>
                        </div>
                        <!-- // Chat END -->

                    </div>
                    <div class="span3 tablet-column-reset">

                        <!-- Activity/List Widget -->
                        <h2 class="margin-none separator bottom"><i
                                class="icon-signal text-primary icon-fixed-width"></i> Activity</h2>
                        <div class="widget widget-heading-simple widget-stats-primary" data-toggle="collapse-widget">
                            <div class="widget-body list">
                                <ul>

                                    <!-- List item -->
                                    <li>
                                        <span>Sales today</span>
                                        <span class="count">&euro;5,900</span>
                                    </li>
                                    <!-- // List item END -->

                                    <!-- List item -->
                                    <li>
                                        <span>Some other stats</span>
                                        <span class="count">36,900</span>
                                    </li>
                                    <!-- // List item END -->

                                    <!-- List item -->
                                    <li>
                                        <span>Some stunning stats</span>
                                        <span class="count">26,999</span>
                                    </li>
                                    <!-- // List item END -->

                                    <!-- List item -->
                                    <li>
                                        <span>Awesome stats</span>
                                        <span class="count">4,900</span>
                                    </li>
                                    <!-- // List item END -->

                                </ul>
                            </div>
                        </div>
                        <!-- // Activity/List Widget END -->
                        <!-- Widget -->
                        <h2 class="margin-none separator bottom"><i
                                class="icon-file-text icon-fixed-width text-primary"></i>Notes</h2>
                        <div class="widget widget-heading-simple widget-body-gray">
                            <div class="widget-body">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu diam eros. Etiam
                                consequat, quam eu molestie tempus, dui elit volutpat massa, at consectetur ligula nunc
                                ac massa. Phasellus quis ante libero, quis accumsan purus. Pellentesque rhoncus accumsan
                                interdu
                            </div>
                        </div>
                        <!-- // Widget END -->


                    </div>
                </div>

            </div>


        </div>
        <!-- // Content END -->
<?php
include_once ROOT_PATH . "/template/menu-footer.php";
?>
   